import 'package:sneakers_shop/data/models/sneakers_model.dart';
import 'package:sneakers_shop/resources/resources.dart';

abstract class MoreData {
  static List<SneakersModel> moreData = [
    SneakersModel(
      sneakersTitle: "NIKE AIR - MAX",
      sneakersPrice: 170.00,
      sneakersImg: Sneakers.nikePink,
    ),
    SneakersModel(
      sneakersTitle: "NIKE AIR FORCE",
      sneakersPrice: 170.20,
      sneakersImg: Sneakers.blackRedWhite,
    ),
    SneakersModel(
      sneakersTitle: "NIKE AIR - MAX",
      sneakersPrice: 170.00,
      sneakersImg: Sneakers.nikePink,
    ),
    SneakersModel(
      sneakersTitle: "AIR JORDAN 5 LANEY JSP",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.jordanBlue,
    ),
    SneakersModel(
      sneakersTitle: "EPIC-REACT",
      sneakersPrice: 130.00,
      sneakersImg: Sneakers.whiteBlue,
    ),
    SneakersModel(
      sneakersTitle: "EPIC-REACT",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.greenBlue,
    ),
    SneakersModel(
      sneakersTitle: "EPIC-REACT",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.red,
    ),
  ];
}
