import 'package:sneakers_shop/data/models/sneakers_model.dart';
import 'package:sneakers_shop/resources/resources.dart';

abstract class CartData {
  static List<SneakersModel> cartData = [
    SneakersModel(
      sneakersTitle: "AIR JORDAN 5 LANEY JSP",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.jordanBlue,
    ),
    SneakersModel(
      sneakersTitle: "HUSTLE-D-9-FLYEASE",
      sneakersPrice: 130.20,
      sneakersImg: Sneakers.blackRedWhite,
    ),
    SneakersModel(
      sneakersTitle: "AIR-MAX-270-BIG-KIDS",
      sneakersPrice: 190.20,
      sneakersImg: Sneakers.blackGreen,
    ),
    SneakersModel(
      sneakersTitle: "AIR JORDAN 5 LANEY JSP",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.jordanBlue,
    ),
  ];
}
