import 'package:flutter/material.dart';

class MainCardModel {
  final String sneakersBrand;
  final String sneakersTitle;
  final double sneakersPrice;
  final String sneakersImg;
  final Color cardColor;

  MainCardModel({
    required this.sneakersBrand,
    required this.sneakersTitle,
    required this.sneakersPrice,
    required this.sneakersImg,
    required this.cardColor,
  });
}
