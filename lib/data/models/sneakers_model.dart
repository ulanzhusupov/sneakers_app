class SneakersModel {
  final String sneakersTitle;
  final double sneakersPrice;
  final String sneakersImg;

  SneakersModel(
      {required this.sneakersTitle,
      required this.sneakersPrice,
      required this.sneakersImg});
}
