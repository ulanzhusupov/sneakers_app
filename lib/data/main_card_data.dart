import 'package:sneakers_shop/data/models/main_card_model.dart';
import 'package:sneakers_shop/presentation/theme/app_colors.dart';
import 'package:sneakers_shop/resources/resources.dart';

abstract class MainCardData {
  static List<MainCardModel> mainCardData = [
    MainCardModel(
      sneakersBrand: "Nike",
      sneakersTitle: "EPIC-REACT",
      sneakersPrice: 130.00,
      sneakersImg: Sneakers.whiteBlue,
      cardColor: AppColors.cardBlue,
    ),
    MainCardModel(
      sneakersBrand: "Nike",
      sneakersTitle: "EPIC-REACT",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.greenBlue,
      cardColor: AppColors.cardGreenBlue,
    ),
    MainCardModel(
      sneakersBrand: "Nike",
      sneakersTitle: "EPIC-REACT",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.red,
      cardColor: AppColors.cardLightOrange,
    ),
    MainCardModel(
      sneakersBrand: "Nike",
      sneakersTitle: "EPIC-REACT",
      sneakersPrice: 190.00,
      sneakersImg: Sneakers.jordanBlue,
      cardColor: AppColors.cardBlue,
    ),
  ];
}
