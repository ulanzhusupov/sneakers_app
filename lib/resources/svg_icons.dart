part of 'resources.dart';

class SvgIcons {
  SvgIcons._();

  static const String heart = 'assets/svg/heart.svg';
  static const String home2 = 'assets/svg/home2.svg';
  static const String location = 'assets/svg/location.svg';
  static const String shoppingcart = 'assets/svg/shoppingcart.svg';
  static const String user = 'assets/svg/user.svg';
}
