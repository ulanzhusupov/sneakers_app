part of 'resources.dart';

class PngIcons {
  PngIcons._();

  static const String home2 = 'assets/png/home2.png';
  static const String iconSeta = 'assets/png/icon_seta.png';
  static const String like = 'assets/png/like.png';
  static const String likeWhite = 'assets/png/like_white.png';
  static const String location = 'assets/png/location.png';
  static const String notification = 'assets/png/notification.png';
  static const String search = 'assets/png/search.png';
  static const String shoppingcart = 'assets/png/shoppingcart.png';
  static const String user = 'assets/png/user.png';
}
