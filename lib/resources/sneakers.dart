part of 'resources.dart';

class Sneakers {
  Sneakers._();

  static const String blackGreen = 'assets/sneakers/blackGreen.png';
  static const String blackRedWhite = 'assets/sneakers/blackRedWhite.png';
  static const String greenBlue = 'assets/sneakers/greenBlue.png';
  static const String jordanBlue = 'assets/sneakers/jordanBlue.png';
  static const String nikePink = 'assets/sneakers/nikePink.png';
  static const String nikeWhite = 'assets/sneakers/nikeWhite.png';
  static const String red = 'assets/sneakers/red.png';
  static const String whiteBlue = 'assets/sneakers/whiteBlue.png';
}
