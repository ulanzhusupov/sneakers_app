import 'package:flutter/material.dart';
import 'package:sneakers_shop/data/models/sneakers_model.dart';

class CartProvider extends ChangeNotifier {
  List<SneakersModel> cartData = [];

  void addToCart(SneakersModel model) {
    cartData.add(model);
    notifyListeners();
  }

  void removeFromCart(SneakersModel model) {
    cartData.remove(model);
    notifyListeners();
  }

  void clearCart() {
    cartData.clear();
    notifyListeners();
  }
}
