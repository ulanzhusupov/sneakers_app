import 'package:flutter/material.dart';
import 'package:sneakers_shop/data/models/sneakers_model.dart';

class AllProvider extends ChangeNotifier {
  int oldIndex = 0;
  int selectedIndex = 0;

  List<SneakersModel> favoriteData = [];

  void changeIndex(int index) {
    oldIndex = selectedIndex;
    selectedIndex = index;
    notifyListeners();
  }

  void changeToOldIndex() {
    selectedIndex = oldIndex;
    notifyListeners();
  }

  void addToFavorites(SneakersModel model) {
    favoriteData.add(model);
    notifyListeners();
  }

  bool isProductInFavorite(String sneakersImg) {
    bool isProductHasInFavorites = false;
    for (var element in favoriteData) {
      isProductHasInFavorites =
          element.sneakersImg.toLowerCase() == sneakersImg.toLowerCase();
    }

    return isProductHasInFavorites;
  }
}
