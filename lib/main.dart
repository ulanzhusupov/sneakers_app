import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sneakers_shop/presentation/screens/home_screen.dart';
import 'package:sneakers_shop/providers/all_provider.dart';
import 'package:sneakers_shop/providers/cart_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AllProvider()),
        ChangeNotifierProvider(create: (context) => CartProvider()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
          fontFamily: "Sansation",
        ),
        home: const HomeScreen(),
      ),
    );
  }
}
