import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s6W700 = TextStyle(
    fontSize: 6,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s9W700 = TextStyle(
    fontSize: 9,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s9W400 = TextStyle(
    fontSize: 9,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s11W400 = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s11W700 = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s16W400 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s20W400 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle s14W700 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s16W700 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s20W700 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s24W700 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle s36W700 = TextStyle(
    fontSize: 36,
    fontWeight: FontWeight.w700,
  );
}
