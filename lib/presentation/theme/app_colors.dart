import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color accentRed = Color(0xffF4446A);
  static const Color buttonRed = Color(0xffEB3B63);
  static const Color cardBlue = Color(0xff537DD7);
  static const Color cardGreenBlue = Color(0xff00ACB9);
  static const Color cardLightOrange = Color(0xffEAA08F);
  static const Color bgGrey = Color(0xffEBEBEB);
  static const Color white = Color(0xffffffff);
  static const Color productText = Color(0xff262626);
  static const Color homeBgBottom = Color(0xffE7EDF0);
  static const Color bottomNavbarUnactive = Color(0xff5C5C5C);
  static const Color bgCartImage = Color(0xffDCE3EA);
  static Color plusMinusBg = const Color(0xff838383).withOpacity(0.2);
}
