import 'package:flutter/material.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';

class BrandsList extends StatelessWidget {
  const BrandsList({
    super.key,
    required this.brands,
  });

  final List brands;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: brands.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, idx) => Padding(
          padding: const EdgeInsets.only(right: 40),
          child: Text(
            brands[idx],
            style: AppFonts.s20W700,
          ),
        ),
      ),
    );
  }
}
