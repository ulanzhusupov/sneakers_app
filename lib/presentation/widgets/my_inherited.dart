import 'package:flutter/material.dart';

class MyInherited extends InheritedWidget {
  const MyInherited({
    super.key,
    required Widget child,
    required this.changeToOldIdx,
    required this.oldIndex,
  }) : super(child: child);

  final Function? changeToOldIdx;
  final int oldIndex;

  // void changeToOldIndex()

  @override
  bool updateShouldNotify(covariant MyInherited oldWidget) {
    return oldWidget.oldIndex != oldIndex;
  }

  // @override
  // bool updateShouldNotify(covariant InheritedWidget oldWidget) {
  //   return true;
  // }
}
