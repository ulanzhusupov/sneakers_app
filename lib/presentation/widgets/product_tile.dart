import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sneakers_shop/presentation/theme/app_colors.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';

class ProductTile extends StatelessWidget {
  const ProductTile({
    super.key,
    required this.productTitle,
    required this.productPrice,
    required this.sneakersImg,
  });
  final String sneakersImg;
  final String productTitle;
  final String productPrice;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Padding(
      padding: EdgeInsets.symmetric(vertical: screenHeight * 0.022),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: screenWidth * 0.4,
            height: screenHeight * 0.16,
            child: Stack(
              alignment: AlignmentDirectional.bottomStart,
              children: [
                Container(
                  width: screenWidth * 0.29,
                  height: screenHeight * 0.127,
                  decoration: BoxDecoration(
                    color: AppColors.bgCartImage,
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
                Positioned(
                  top: -30,
                  right: -15,
                  child: Image.asset(
                    sneakersImg,
                    width: screenWidth * 0.5,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: screenWidth * 0.01),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                productTitle,
                style: AppFonts.s14W700.copyWith(fontSize: screenWidth * 0.032),
              ),
              SizedBox(height: screenHeight * 0.008),
              Text(
                productPrice,
                style: AppFonts.s24W700.copyWith(fontSize: screenWidth * 0.055),
              ),
              SizedBox(height: screenHeight * 0.02),
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: AppColors.plusMinusBg,
                    child: IconButton(
                      onPressed: () {},
                      icon: const Icon(CupertinoIcons.minus),
                    ),
                  ),
                  SizedBox(width: screenWidth * 0.039),
                  const Text(
                    "0",
                    style: AppFonts.s14W700,
                  ),
                  SizedBox(width: screenWidth * 0.039),
                  CircleAvatar(
                    backgroundColor: AppColors.plusMinusBg,
                    child: IconButton(
                      onPressed: () {},
                      icon: const Icon(CupertinoIcons.plus),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
