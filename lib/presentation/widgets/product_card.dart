import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sneakers_shop/data/models/sneakers_model.dart';
import 'package:sneakers_shop/presentation/theme/app_colors.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';
import 'package:sneakers_shop/providers/all_provider.dart';
import 'package:sneakers_shop/providers/cart_provider.dart';
import 'package:sneakers_shop/resources/resources.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({super.key, required this.sneakersModel});

  final SneakersModel sneakersModel;

  bool isProductInFavorite(List<SneakersModel> favorites) {
    bool isProductHasInFavorites = false;
    for (var element in favorites) {
      isProductHasInFavorites = element.sneakersImg.toLowerCase() ==
          sneakersModel.sneakersImg.toLowerCase();
    }

    return isProductHasInFavorites;
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final favoriteProvider = context.watch<AllProvider>();
    final cartProvider = context.watch<CartProvider>();

    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: screenWidth * 0.017, vertical: screenHeight * 0.007),
      child: SizedBox(
        width: screenWidth * 0.42,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: screenHeight * 0.30,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(9),
                  color: AppColors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      blurRadius: 5,
                      spreadRadius: 0,
                      offset: Offset(0, 5),
                    ),
                  ]),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  vertical: screenHeight * 0.01,
                  horizontal: screenWidth * 0.03,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      color: AppColors.accentRed,
                      child: RotatedBox(
                        quarterTurns: -1,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: screenHeight * 0.023,
                          ),
                          child: Text(
                            "New",
                            style: AppFonts.s6W700.copyWith(
                              color: AppColors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        favoriteProvider.addToFavorites(sneakersModel);
                      },
                      child: isProductInFavorite(favoriteProvider.favoriteData)
                          ? Icon(
                              CupertinoIcons.heart_fill,
                              color: AppColors.accentRed,
                              size: screenWidth * 0.055,
                            )
                          : Image.asset(
                              PngIcons.like,
                              width: screenWidth * 0.055,
                            ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 0,
              child: Image.asset(
                sneakersModel.sneakersImg,
                width: screenWidth * 0.39,
                height: screenHeight * 0.15,
              ),
            ),
            Positioned(
              bottom: 50,
              child: Column(
                children: [
                  Text(
                    sneakersModel.sneakersTitle,
                    style: AppFonts.s11W700.copyWith(
                      color: AppColors.productText,
                    ),
                  ),
                  SizedBox(height: screenHeight * 0.005),
                  Text(
                    "${sneakersModel.sneakersPrice}",
                    style: AppFonts.s9W400.copyWith(
                      color: AppColors.productText,
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 10,
              child: SizedBox(
                height: screenHeight * 0.03,
                child: ElevatedButton(
                  onPressed: () {
                    cartProvider.cartData.any(
                            (e) => e.sneakersImg == sneakersModel.sneakersImg)
                        ? cartProvider.removeFromCart(sneakersModel)
                        : cartProvider.addToCart(sneakersModel);
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: AppColors.accentRed,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  child: cartProvider.cartData.any(
                          (e) => e.sneakersImg == sneakersModel.sneakersImg)
                      ? Row(
                          children: [
                            const Icon(
                              CupertinoIcons.check_mark,
                              color: AppColors.white,
                              size: 15,
                            ),
                            const SizedBox(width: 5),
                            Text(
                              "Added",
                              style: AppFonts.s9W400
                                  .copyWith(color: AppColors.white),
                            ),
                          ],
                        )
                      : Row(
                          children: [
                            const Icon(
                              CupertinoIcons.cart,
                              color: AppColors.white,
                              size: 15,
                            ),
                            const SizedBox(width: 5),
                            Text(
                              "Add to cart",
                              style: AppFonts.s9W400
                                  .copyWith(color: AppColors.white),
                            ),
                          ],
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
