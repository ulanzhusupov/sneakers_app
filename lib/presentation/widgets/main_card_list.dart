import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sneakers_shop/data/main_card_data.dart';
import 'package:sneakers_shop/providers/all_provider.dart';
import 'package:sneakers_shop/presentation/widgets/main_card.dart';

class MainCardList extends StatelessWidget {
  const MainCardList({
    super.key,
    required this.screenWidth,
  });

  final double screenWidth;

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<AllProvider>();
    return SizedBox(
      width: screenWidth * 0.75,
      height: 343,
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: MainCardData.mainCardData.length,
        itemBuilder: (context, index) => MainCard(
          isAddedToFavorites: vm.isProductInFavorite(
              MainCardData.mainCardData[index].sneakersImg),
          mainCardModel: MainCardData.mainCardData[index],
        ),
      ),
    );
  }
}
