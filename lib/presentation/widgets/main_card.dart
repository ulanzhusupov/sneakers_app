import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sneakers_shop/data/models/main_card_model.dart';
import 'package:sneakers_shop/data/models/sneakers_model.dart';
import 'package:sneakers_shop/presentation/theme/app_colors.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';
import 'package:sneakers_shop/providers/all_provider.dart';
import 'package:sneakers_shop/resources/resources.dart';

class MainCard extends StatelessWidget {
  const MainCard({
    super.key,
    required this.mainCardModel,
    required this.isAddedToFavorites,
  });

  final MainCardModel mainCardModel;
  final bool isAddedToFavorites;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    final vm = context.watch<AllProvider>();

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
      child: SizedBox(
        width: screenWidth * 0.67,
        height: 343,
        child: Stack(
          children: [
            Container(
              width: screenWidth * 0.568,
              height: 343,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: mainCardModel.cardColor,
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: screenWidth * 0.04,
                  vertical: 14,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          mainCardModel.sneakersBrand,
                          style: AppFonts.s20W400.copyWith(
                            color: AppColors.white,
                            fontSize: screenWidth * 0.046,
                          ),
                        ),
                        Text(
                          mainCardModel.sneakersTitle,
                          style: AppFonts.s24W700.copyWith(
                            color: AppColors.white,
                            fontSize: screenWidth * 0.055,
                          ),
                        ),
                        Text(
                          "${mainCardModel.sneakersPrice}",
                          style: AppFonts.s16W400.copyWith(
                            color: AppColors.white,
                            fontSize: screenWidth * 0.037,
                          ),
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        vm.addToFavorites(SneakersModel(
                          sneakersTitle: mainCardModel.sneakersTitle,
                          sneakersPrice: mainCardModel.sneakersPrice,
                          sneakersImg: mainCardModel.sneakersImg,
                        ));
                      },
                      child: isAddedToFavorites
                          ? const Icon(
                              CupertinoIcons.heart_fill,
                              color: AppColors.accentRed,
                              size: 27,
                            )
                          : Image.asset(
                              PngIcons.likeWhite,
                              width: 27,
                            ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              top: 40,
              right: -30,
              child: Image.asset(
                mainCardModel.sneakersImg,
                width: screenWidth * 0.8,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
