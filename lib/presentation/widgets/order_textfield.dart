import 'package:flutter/material.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';

class OrderTextField extends StatelessWidget {
  const OrderTextField({
    super.key,
    this.keyboardType,
    required this.hintText,
    required this.controller,
  });

  final TextInputType? keyboardType;
  final String hintText;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        fillColor: Colors.white,
        filled: true,
        hintText: hintText,
        hintStyle: AppFonts.s16W400,
        border: InputBorder.none,
      ),
    );
  }
}
