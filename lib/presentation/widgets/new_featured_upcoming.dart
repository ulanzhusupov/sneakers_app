import 'package:flutter/material.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';

class NewFeaturedUpcoming extends StatelessWidget {
  const NewFeaturedUpcoming({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        RotatedBox(
          quarterTurns: -1,
          child: Text(
            "Upcoming",
            style: AppFonts.s16W700,
          ),
        ),
        SizedBox(height: 63),
        RotatedBox(
          quarterTurns: -1,
          child: Text(
            "Featured",
            style: AppFonts.s16W700,
          ),
        ),
        SizedBox(height: 63),
        RotatedBox(
          quarterTurns: -1,
          child: Text(
            "New",
            style: AppFonts.s16W700,
          ),
        ),
      ],
    );
  }
}
