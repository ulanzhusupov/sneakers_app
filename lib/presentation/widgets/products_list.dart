import 'package:flutter/material.dart';
import 'package:sneakers_shop/data/more_data.dart';
import 'package:sneakers_shop/presentation/widgets/product_card.dart';

class ProductsList extends StatelessWidget {
  const ProductsList({
    super.key,
    required this.screenWidth,
    required this.screenHeight,
  });

  final double screenWidth;
  final double screenHeight;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: screenWidth,
      height: MediaQuery.of(context).size.height,
      child: GridView.builder(
        scrollDirection: Axis.vertical,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, // Указываем количество столбцов
          childAspectRatio: (screenWidth * 0.42) / (screenHeight * 0.25),
        ),
        physics: const NeverScrollableScrollPhysics(),
        itemCount: MoreData.moreData.length,
        itemBuilder: (context, index) => ProductCard(
          sneakersModel: MoreData.moreData[index],
        ),
      ),
    );
  }
}
