// ignore_for_file: deprecated_member_use

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:sneakers_shop/presentation/screens/cart_screen.dart';
import 'package:sneakers_shop/presentation/screens/favorite_screen.dart';
import 'package:sneakers_shop/presentation/screens/main_screen.dart';
import 'package:sneakers_shop/presentation/theme/app_colors.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';
import 'package:sneakers_shop/providers/all_provider.dart';
import 'package:sneakers_shop/resources/resources.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static final List<Widget> _widgetOptions = <Widget>[
    const MainScreen(),
    const FavoriteScreen(),
    const Text(
      'Index 2: School',
      style: AppFonts.s16W400,
    ),
    const CartScreen(),
    const Text(
      'Index 4: School',
      style: AppFonts.s16W400,
    ),
  ];

  void onItemTapped(int index) {}

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<AllProvider>();
    return Scaffold(
      body: _widgetOptions[vm.selectedIndex],
      bottomNavigationBar: vm.selectedIndex != 3
          ? BottomNavigationBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    SvgIcons.home2,
                    color: AppColors.bottomNavbarUnactive,
                  ),
                  activeIcon: SvgPicture.asset(
                    SvgIcons.home2,
                    color: AppColors.accentRed,
                  ),
                  label: '',
                ),
                BottomNavigationBarItem(
                  icon: Stack(
                    children: [
                      SvgPicture.asset(
                        SvgIcons.heart,
                        color: AppColors.bottomNavbarUnactive,
                      ),
                      vm.favoriteData.isNotEmpty
                          ? Positioned(
                              bottom: 0,
                              right: 0,
                              child: CircleAvatar(
                                radius: 10,
                                backgroundColor: AppColors.accentRed,
                                child: Text(
                                  vm.favoriteData.length.toString(),
                                  style: AppFonts.s9W700.copyWith(
                                    color: AppColors.white,
                                  ),
                                ),
                              ),
                            )
                          : const SizedBox()
                    ],
                  ),
                  activeIcon: Stack(
                    children: [
                      SvgPicture.asset(
                        SvgIcons.heart,
                        color: AppColors.bottomNavbarUnactive,
                      ),
                      vm.favoriteData.isNotEmpty
                          ? Positioned(
                              bottom: 0,
                              right: 0,
                              child: CircleAvatar(
                                radius: 8.5,
                                backgroundColor: AppColors.accentRed,
                                child: Text(
                                  vm.favoriteData.length.toString(),
                                  style: AppFonts.s9W700.copyWith(
                                    color: AppColors.white,
                                  ),
                                ),
                              ),
                            )
                          : const SizedBox()
                    ],
                  ),
                  label: '',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    SvgIcons.location,
                    color: AppColors.bottomNavbarUnactive,
                  ),
                  activeIcon: SvgPicture.asset(
                    SvgIcons.location,
                    color: AppColors.accentRed,
                  ),
                  label: '',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    SvgIcons.shoppingcart,
                    color: AppColors.bottomNavbarUnactive,
                  ),
                  activeIcon: SvgPicture.asset(
                    SvgIcons.shoppingcart,
                    color: AppColors.accentRed,
                  ),
                  label: '',
                ),
                BottomNavigationBarItem(
                  icon: SvgPicture.asset(
                    SvgIcons.user,
                    color: AppColors.bottomNavbarUnactive,
                  ),
                  activeIcon: SvgPicture.asset(
                    SvgIcons.user,
                    color: AppColors.accentRed,
                  ),
                  label: '',
                ),
              ],
              currentIndex: vm.selectedIndex,
              onTap: (idx) {
                vm.changeIndex(idx);
              },
            )
          : const SizedBox.shrink(),
    );
  }
}
