import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sneakers_shop/core/app_consts.dart';
import 'package:sneakers_shop/core/network/dio_settings.dart';
import 'package:sneakers_shop/data/models/email_model.dart';
import 'package:sneakers_shop/data/models/sneakers_model.dart';
import 'package:sneakers_shop/presentation/theme/app_colors.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';
import 'package:sneakers_shop/presentation/widgets/order_textfield.dart';
import 'package:sneakers_shop/providers/all_provider.dart';
import 'package:sneakers_shop/presentation/widgets/product_tile.dart';
import 'package:sneakers_shop/providers/cart_provider.dart';
import 'package:sneakers_shop/resources/resources.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController phoneController = TextEditingController();

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final allProvider = context.watch<AllProvider>();
    final cartProvider = context.watch<CartProvider>();
    double totalPrice = cartProvider.cartData.isNotEmpty
        ? cartProvider.cartData
            .map((e) => e.sneakersPrice)
            .reduce((v, e) => v + e)
        : 0.0;

    return WillPopScope(
      onWillPop: () {
        allProvider.changeToOldIndex();
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            padding: EdgeInsets.only(left: screenWidth * 0.065),
            onPressed: allProvider.changeToOldIndex,
            icon: Image.asset(
              PngIcons.iconSeta,
              width: screenWidth * 0.062,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.065),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "My Bag",
                      style: AppFonts.s36W700,
                    ),
                    Text(
                      "Total ${cartProvider.cartData.length} Items",
                      style: AppFonts.s16W700,
                    ),
                  ],
                ),
              ),
              SizedBox(height: screenHeight * 0.03),
              const Divider(
                height: 0,
                indent: 0,
                thickness: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.065),
                child: SizedBox(
                  height: screenHeight * 0.62,
                  child: ListView.builder(
                    physics: const BouncingScrollPhysics(),
                    itemCount: cartProvider.cartData.length,
                    itemBuilder: (context, index) => ProductTile(
                      productTitle: cartProvider.cartData[index].sneakersTitle,
                      productPrice:
                          "\$${cartProvider.cartData[index].sneakersPrice}",
                      sneakersImg: cartProvider.cartData[index].sneakersImg,
                    ),
                    scrollDirection: Axis.vertical,
                  ),
                ),
              ),
              const Divider(
                height: 0,
                indent: 0,
                thickness: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.065),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text("Total", style: AppFonts.s16W700),
                        Text("\$$totalPrice", style: AppFonts.s24W700),
                      ],
                    ),
                    SizedBox(height: screenHeight * 0.031),
                    SizedBox(
                      width: double.infinity,
                      height: screenHeight * 0.049,
                      child: ElevatedButton(
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  backgroundColor: AppColors.white,
                                  title: const Text(
                                    "Order sneakers",
                                    style: AppFonts.s20W700,
                                  ),
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      OrderTextField(
                                        keyboardType: TextInputType.name,
                                        hintText: "Name",
                                        controller: nameController,
                                      ),
                                      SizedBox(height: screenHeight * 0.02),
                                      OrderTextField(
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        hintText: "Email",
                                        controller: emailController,
                                      ),
                                      SizedBox(height: screenHeight * 0.02),
                                      OrderTextField(
                                        keyboardType: TextInputType.phone,
                                        hintText: "Phone number",
                                        controller: phoneController,
                                      ),
                                      SizedBox(height: screenHeight * 0.02),
                                      ElevatedButton(
                                        onPressed: () {
                                          sendOrder(
                                                  nameController.text,
                                                  phoneController.text,
                                                  emailController.text,
                                                  cartProvider.cartData,
                                                  totalPrice)
                                              .then((value) => showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return AlertDialog.adaptive(
                                                      content: const Text(
                                                        "Success",
                                                        style: AppFonts.s16W700,
                                                      ),
                                                      actions: <Widget>[
                                                        Align(
                                                          alignment: Alignment
                                                              .topRight,
                                                          child: IconButton(
                                                            icon: const Icon(
                                                              CupertinoIcons
                                                                  .xmark,
                                                            ),
                                                            onPressed: () {
                                                              Navigator.pop(
                                                                  context);
                                                            },
                                                          ),
                                                        ),
                                                      ],
                                                    );
                                                  }))
                                              .then((value) =>
                                                  Navigator.pop(context));
                                        },
                                        style: ElevatedButton.styleFrom(
                                          backgroundColor: AppColors.accentRed,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                          ),
                                        ),
                                        child: Text(
                                          "Send",
                                          style: AppFonts.s14W700
                                              .copyWith(color: AppColors.white),
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              });
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.accentRed,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        child: Text(
                          "BUY",
                          style:
                              AppFonts.s14W700.copyWith(color: AppColors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> sendOrder(String toName, String phoneNumber, String email,
      List<SneakersModel> sneakers, double totalPrice) async {
    final dio = DioSettings().dio;
    try {
      await dio.post(
        "https://api.emailjs.com/api/v1.0/email/send",
        data: EmailModel(
          serviceId: AppConsts.serviceId,
          templateId: AppConsts.templateId,
          userId: AppConsts.userId,
          accessToken: AppConsts.accessToken,
          templateParams: TemplateParams(
              fromName: "Sneakers App",
              toName: toName,
              replyTo: "Sneakers team",
              message:
                  "New order by $toName\nPhone number: $phoneNumber\nCustomer email: $email\nOrder list:\n${sneakers.map((e) => "${e.sneakersTitle} \$${e.sneakersPrice}").toList().join("\n")}\nTotal price: $totalPrice"),
        ).toJson(),
      );
    } catch (e) {
      String errorText = e.toString();
      if (e is DioException) {
        errorText = e.response?.data;
      }
      print(errorText);
    }
  }
}
