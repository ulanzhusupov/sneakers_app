import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';
import 'package:sneakers_shop/providers/all_provider.dart';
import 'package:sneakers_shop/presentation/widgets/product_tile.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    final vm = context.watch<AllProvider>();

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.065),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Favorites",
                    style: AppFonts.s36W700,
                  ),
                  Text(
                    "Total ${vm.favoriteData.length} Items",
                    style: AppFonts.s16W700,
                  ),
                ],
              ),
            ),
            SizedBox(height: screenHeight * 0.03),
            const Divider(
              height: 0,
              indent: 0,
              thickness: 1,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.065),
              child: SizedBox(
                height: screenHeight * 0.78,
                child: ListView.builder(
                  physics: const BouncingScrollPhysics(),
                  itemCount: vm.favoriteData.length,
                  itemBuilder: (context, index) => ProductTile(
                    productTitle: vm.favoriteData[index].sneakersTitle,
                    productPrice: "\$${vm.favoriteData[index].sneakersPrice}",
                    sneakersImg: vm.favoriteData[index].sneakersImg,
                  ),
                  scrollDirection: Axis.vertical,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
