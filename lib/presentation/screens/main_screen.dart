import 'package:flutter/material.dart';
import 'package:sneakers_shop/presentation/theme/app_colors.dart';
import 'package:sneakers_shop/presentation/theme/app_fonts.dart';
import 'package:sneakers_shop/presentation/widgets/brands_list.dart';
import 'package:sneakers_shop/presentation/widgets/main_card_list.dart';
import 'package:sneakers_shop/presentation/widgets/new_featured_upcoming.dart';
import 'package:sneakers_shop/presentation/widgets/products_list.dart';
import 'package:sneakers_shop/resources/resources.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    List brands = [
      "Nike",
      "Adidas",
      "Jordan",
      "Puma",
      "Reebook",
    ];

    return Scaffold(
      backgroundColor: AppColors.homeBgBottom,
      appBar: AppBar(
        backgroundColor: AppColors.white,
        surfaceTintColor: AppColors.homeBgBottom,
        elevation: 0,
        title: Padding(
          padding: EdgeInsets.only(left: screenWidth * 0.02),
          child: const Text(
            "Discover",
            style: AppFonts.s36W700,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            style: IconButton.styleFrom(
              backgroundColor: AppColors.bgGrey,
            ),
            icon: Image.asset(
              PngIcons.search,
              width: 20,
            ),
          ),
          IconButton(
            onPressed: () {},
            style: IconButton.styleFrom(
              backgroundColor: AppColors.bgGrey,
            ),
            icon: Image.asset(
              PngIcons.notification,
              width: 20,
            ),
          ),
          SizedBox(width: screenWidth * 0.02),
        ],
      ),
      body: Stack(
        children: [
          Container(
            width: screenWidth,
            height: screenHeight * 0.74,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(screenWidth * 0.2),
                bottomRight: Radius.circular(screenWidth * 0.2),
              ),
              color: AppColors.white,
            ),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: screenWidth * 0.06),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 47),
                BrandsList(brands: brands),
                const SizedBox(height: 27),
                Row(
                  children: [
                    const NewFeaturedUpcoming(),
                    SizedBox(width: screenWidth * 0.02),
                    MainCardList(screenWidth: screenWidth),
                  ],
                ),
                SizedBox(height: screenHeight * 0.03),
                const Text("More", style: AppFonts.s20W700),
                SizedBox(height: screenHeight * 0.017),
                ProductsList(
                  screenWidth: screenWidth,
                  screenHeight: screenHeight,
                ),
                // SizedBox(height: screenHeight * 0.017),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
