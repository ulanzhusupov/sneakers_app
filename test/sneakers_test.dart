import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:sneakers_shop/resources/resources.dart';

void main() {
  test('sneakers assets test', () {
    expect(File(Sneakers.blackGreen).existsSync(), isTrue);
    expect(File(Sneakers.blackRedWhite).existsSync(), isTrue);
    expect(File(Sneakers.greenBlue).existsSync(), isTrue);
    expect(File(Sneakers.jordanBlue).existsSync(), isTrue);
    expect(File(Sneakers.nikePink).existsSync(), isTrue);
    expect(File(Sneakers.nikeWhite).existsSync(), isTrue);
    expect(File(Sneakers.red).existsSync(), isTrue);
    expect(File(Sneakers.whiteBlue).existsSync(), isTrue);
  });
}
