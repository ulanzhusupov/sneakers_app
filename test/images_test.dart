import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:sneakers_shop/resources/resources.dart';

void main() {
  test('images assets test', () {
    expect(File(Images.mainBg).existsSync(), isTrue);
  });
}
