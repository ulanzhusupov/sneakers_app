import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:sneakers_shop/resources/resources.dart';

void main() {
  test('svg_icons assets test', () {
    expect(File(SvgIcons.heart).existsSync(), isTrue);
    expect(File(SvgIcons.home2).existsSync(), isTrue);
    expect(File(SvgIcons.location).existsSync(), isTrue);
    expect(File(SvgIcons.shoppingcart).existsSync(), isTrue);
    expect(File(SvgIcons.user).existsSync(), isTrue);
  });
}
