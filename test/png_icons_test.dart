import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:sneakers_shop/resources/resources.dart';

void main() {
  test('png_icons assets test', () {
    expect(File(PngIcons.home2).existsSync(), isTrue);
    expect(File(PngIcons.iconSeta).existsSync(), isTrue);
    expect(File(PngIcons.like).existsSync(), isTrue);
    expect(File(PngIcons.likeWhite).existsSync(), isTrue);
    expect(File(PngIcons.location).existsSync(), isTrue);
    expect(File(PngIcons.notification).existsSync(), isTrue);
    expect(File(PngIcons.search).existsSync(), isTrue);
    expect(File(PngIcons.shoppingcart).existsSync(), isTrue);
    expect(File(PngIcons.user).existsSync(), isTrue);
  });
}
